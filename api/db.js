var faker = require('faker')

module.exports = () => {

  const data = { posts: [] }

  for (let i = 0; i < 5; i++) {

    data.posts.push({
      id: i,
      title: `${faker.lorem.words(4)}`,
      content: `${faker.lorem.sentence()}`,
    })
  }

  return data
}